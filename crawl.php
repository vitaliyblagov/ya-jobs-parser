<?php
include "simple_html_dom.php";
error_reporting(0);

switch($_GET['action']){
    case "getparams":
        header('Content-Type: application/json');
        $html = file_get_html('https://yandex.ru/jobs');
        echo json_encode(getParams($html));
        break;


    case "getvacs":
        header('Content-Type: application/json');
        if($_GET['url'] != "*") {
            $html = file_get_html('https://yandex.ru'.$_GET['url']."?cities=".$_GET['city']);
            echo json_encode(getVacs($html, $_GET['city']));
        }
        else{
            echo json_encode(getAll($_GET['city']));
        }
        break;

    case "getxls":
        if($_GET['url'] != "*") {
            $html = file_get_html('https://yandex.ru'.$_GET['url']."?cities=".$_GET['city']);
            convToXL(getVacs($html, $_GET['city']));
        }
        else{
            convToXL(getAll($_GET['city']));
        }
        break;

}

function getParams($html)
{
    $cities = array();
    foreach ($html->find("span.page-content__city-select select option") as $city)
        $cities[$city->plaintext] = $city->value;

    $vacURLs = array();
    foreach ($html->find("table[class*=specs-table__table] tbody tr td a") as $vactype)
        $vacURLs[$vactype->plaintext] = $vactype->href;
    $vacURLs["Все категории"] = "*";

    return array("cities" => $cities, "cats" => $vacURLs);
}

function getVacs($html, $city){
    $done = array(array("Название вакансии", "Город", "Ссылка"));
    foreach($html->find('.serp__item') as $vac) {
        $databem_json = json_decode(html_entity_decode($vac->attr["data-bem"]));
        if($databem_json->serp__item->tags->cities[0] == $city || $databem_json->serp__item->tags->cities[1] == $city || $city == "")
            array_push($done, array($vac->children(0)->plaintext, $vac->children(1)->children(0)->plaintext, "http://yandex.ru" . $vac->children(0)->href));
    }
    //print_r($done);
    return $done;
}

function getAll($city){
    $html = file_get_html('https://yandex.ru/jobs');
    $params = getParams($html);
    $result = array(array("Название вакансии", "Город", "Ссылка"));
    foreach($params["cats"] as $cat){
        if ($cat != "*") {
            $html = file_get_html('https://yandex.ru' . $cat . "?cities=" . $city);
            $result += getVacs($html, $city);
        }
    }
    return $result;

}

function convToXL($content){
    header("Content-type: text/csv; charset=windows-1251");
    header("Content-Disposition: attachment; filename=file.csv");
    header("Pragma: no-cache");
    header("Expires: 0");
    $out = fopen("php://output", 'w');

    foreach ($content as $data)
    {
        foreach($data as $p=>$cell){
            $data[$p] = iconv( "utf-8", "windows-1251", $cell);
        }
        fputcsv($out, $data,";");
    }
    fclose($out);
}

die();

?>